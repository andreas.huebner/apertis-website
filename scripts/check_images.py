#!/usr/bin/env python3

# Check links to images.

import markdown
import os
import re
import sys

def get_link(url):
    link = url.group("link")

    if not link[:8] == "/images/":
        return

    if not link[8:] in links:
        links.append(link[8:])


def parse_file(filename):
    # print("%s: " % filename)
    with open(filename, "r+") as file:
        contents = file.read()

        if not contents[0:3] == "+++":
            return

        fm = contents.split("+++")[1]
        doc = contents.split("+++", 2)[2]

        # Convert to HTML to simplify link detection
        text = markdown.markdown(doc)

        # From img tags
        pattern = re.compile("src=\"(?P<link>.*?)\"")
        doc = pattern.sub(get_link, text)

        # We have some links to bits in images
        pattern = re.compile("href=\"(?P<link>.*?)\"")
        doc = pattern.sub(get_link, text)


images = []

# List of files in images folder
for root, dirs, files in os.walk("%s/static/images" % (sys.argv[1])):
    for file in files:
        images.append(file)

links = []

# Parse aliases
for root, dirs, files in os.walk("%s/content" % (sys.argv[1])):
    for file in files:
        if ".md" in file:
            try:
                parse_file("%s/%s" % (root, file))
            except:
                print("Failed to parse %s/%s" % (root, file))

broken = []
for link in links:
    sys.stdout.flush()

    if link in images:
        images.remove(link)
    else:
        broken.append(link)

print(f"Found {len(broken)} broken image links in {len(links)} tested:")
broken.sort()
for b in broken:
    print(" ", b)

print(f"Found {len(images)} unused images:")
images.sort()
for i in images:
    print(" ", i)

sys.exit(1 if broken or images else 0)
