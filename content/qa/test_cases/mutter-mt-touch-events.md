+++
date = "2015-09-11"
weight = 100

title = "mutter-mt-touch-events"

aliases = [
    "/old-wiki/QA/Test_Cases/mutter-mt-touch-events"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
