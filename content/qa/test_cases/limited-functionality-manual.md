+++
date = "2017-10-25"
weight = 100

title = "limited-functionality-manual"

aliases = [
    "/old-wiki/QA/Test_Cases/limited-functionality-manual"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
