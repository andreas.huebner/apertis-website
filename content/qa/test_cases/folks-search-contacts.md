+++
date = "2018-06-25"
weight = 100

title = "folks-search-contacts"

aliases = [
    "/qa/test_cases/folks-search.md",
    "/old-wiki/QA/Test_Cases/folks-search",
    "/old-wiki/QA/Test_Cases/folks-search-contacts"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
