+++
date = "2017-10-23"
weight = 100

title = "clutter-mt-rotate"

aliases = [
    "/old-wiki/QA/Test_Cases/clutter-mt-rotate"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
