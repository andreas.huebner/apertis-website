+++
date = "2017-10-23"
weight = 100

title = "clutter-actors-bigger-reactive-area"

aliases = [
    "/old-wiki/QA/Test_Cases/clutter-actors-bigger-reactive-area"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
