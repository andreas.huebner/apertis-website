+++
date = "2017-10-23"
weight = 100

title = "clutter-i18-gettext"

aliases = [
    "/old-wiki/QA/Test_Cases/clutter-i18-gettext"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
