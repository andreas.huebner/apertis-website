+++
date = "2020-06-10"
weight = 100

title = "Optional Extra Reference Hardware"
+++

The following optional extras are those used for testing in combination with
the reference hardware platforms.

| Reference                    | Hardware                                                                                         | Comments                                                                                                                     |
| ---------------------------- | ------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------- |
| Bluetooth dongle             | [Bluetooth PTS Radio Module](https://store.bluetooth.com/)                                       | Only needed for Bluetooth testing and development.                                                                           |
| Reference phone \#1          | iPhone 6                                                                                         | Only needed for Bluetooth testing and development.                                                                           |
| Reference phone \#2          | Android Phone                                                                                    | Only needed for Bluetooth testing and development. Please use any Android phone with Bluetooth v4.0 and Android OS version 9 |
| Reference router             | [D-link N300 router](https://eu.dlink.com/uk/en/products/dir-615-wireless-n-300-router)          | Only needed for ConnMan testing and development.                                                                             |
| Reference 3G modem           | [Huawei E3372 3G modem](https://consumer.huawei.com/en/routers/e3372/specs/)                     | Only needed for 3G connectivity testing and development.                                                                     |
| SDK Multi-touch device       | [Apple Magic Trackpad](http://store.apple.com/us/product/MC380LL/A/magic-trackpad)               | Only needed for multi-touch development.                                                                                     |
| Reference Webcam             | [Logitech HD Pro Webcam C910](http://www.logitech.com/en-in/product/6816)                        | Only needed for development related with camera features.                                                                    |
| Alternative Bluetooth dongle | [Asus USB-BT211 Mini Bluetooth Dongle](http://www.amazon.co.uk/gp/product/B0033CA2XM)            | Only needed for development related to BlueTooth.                                                                            |
| Wireless dongle              | [Asus USB-N10 NANO - 150Mbps Wireless Lan USB Adapter](https://www.asus.com/uk/Networking-IoT-Servers/Adapters/All-series/USBN10_NANO) | Only needed for development related to Wireless. Supports WPA3/SAE security.           |

